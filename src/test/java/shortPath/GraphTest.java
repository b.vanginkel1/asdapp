package shortPath;

import org.junit.Before;
import org.junit.Test;
import shortPath.pathalgorithms.PathFinder;
import shortPath.pathalgorithms.UnweightedPathFinder;
import shortPath.pathalgorithms.DijkstraPathFinder;

import static org.junit.Assert.*;

public class GraphTest {

    /**
     *     -> C
     *   A      -> D
     *     -> B
     */
    private Graph abcdGrah;

    /**
     *     -> C
     *   A      -> D  <- E
     *     -> B
     *  /\
     *  |
     *  F
     */
    private Graph abcdefGrah;

    /**
     *    a -> b
     *
     *    q
     */
    private Graph abqGrah;

    @Before
    public void setUp() {

        //minimal
        Graph abcgraaf = new Graph();
        abcgraaf.addEdge("A", "B", 3);
        abcgraaf.addEdge("B", "C", 6);
        abcgraaf.addEdge("A", "C", 2);
        abc = abcgraaf;

        abcdGrah = new Graph();
        abcdGrah.addEdge("A", "B", 3);
        abcdGrah.addEdge("B", "D", 6);
        abcdGrah.addEdge("A", "C", 6);
        abcdGrah.addEdge("C", "D", 2);

        abcdefGrah = new Graph();
        abcdefGrah.addEdge("A", "B", 3);
        abcdefGrah.addEdge("B", "D", 6);
        abcdefGrah.addEdge("A", "C", 6);
        abcdefGrah.addEdge("C", "D", 2);
        abcdefGrah.addEdge("F", "A", 5);
        abcdefGrah.addEdge("E", "D", 4);

        abqGrah = new Graph();
        abqGrah.addEdge("A", "B", 10);
        abqGrah.getAndAddNode("Q");
    }

    @Test
    public void testIsConnectedStrong() {
        assertTrue(abcdGrah.isConnected());
    }

    @Test
    public void testIsConnectedWeak() {
        assertTrue(abcdefGrah.isConnected());
    }

    @Test
    public void testIsConnectedNot() {
        assertFalse(abqGrah.isConnected());
    }

    private Graph abc;

    @Test
    public void addToGraph()
    {
        Graph g = new Graph();
        g.addEdge("A", "B", 1);
        g.addEdge("A", "D", 1);
        g.addEdge("B", "C", 1);
        g.addEdge("C", "A", 1);
        Node node = g.getNode("A");
        assertEquals("A", node.name);
        assertEquals(2, node.adj.size());
    }

    @Test
    public void findPathUnweightedMin() throws Exception{
        UnweightedPathFinder finder = new UnweightedPathFinder(abc);
        finder.findPath(abc.getNode("A"));
        assertEquals("A->2->C", finder.getPathString(abc.getNode("C")));
    }

    @Test
    public void findPathWeightedMin() throws Exception{
        DijkstraPathFinder finder = new DijkstraPathFinder(abc);
        finder.findPath(abc.getNode("A"));
        assertEquals("A->2->C", finder.getPathString(abc.getNode("C")));
    }


    private Graph generateAbcd()
    {
        Graph abcdgraaf = new Graph();
        abcdgraaf.addEdge("A", "B", 3);
        abcdgraaf.addEdge("B", "D", 6);
        abcdgraaf.addEdge("A", "C", 6);
        abcdgraaf.addEdge("C", "D", 2);
        return abcdgraaf;
    }

    @Test
    public void findPathWeighted() throws Exception {
        Graph abcd = generateAbcd();
        DijkstraPathFinder finder = new DijkstraPathFinder(abcd);
        finder.findPath(abcd.getNode("A"));
        assertEquals("A->6->C->2->D", finder.getPathString(abcd.getNode("D")));
    }

    @Test
    public void findPathUnweighted() throws Exception {
        Graph abcd = generateAbcd();
        UnweightedPathFinder finder = new UnweightedPathFinder(abcd);
        finder.findPath(abcd.getNode("A"));

        assertEquals("A->3->B->6->D", finder.getPathString(abcd.getNode("D")));
    }

    /**
     * unconnected graph
     * @return Graph
     */
    private Graph generateAbce()
    {
        Graph abcegraaf = new Graph();
        abcegraaf.addEdge("A", "B", 3);
        abcegraaf.addEdge("B", "C", 6);
        abcegraaf.addEdge("D", "E", 2);
        return abcegraaf;
    }

    @Test(expected = PathFinder.PathNotFoundException.class)
    public void PathNotFoundUnweighted() throws Exception {
        Graph abce = generateAbce();
        UnweightedPathFinder finder = new UnweightedPathFinder(abce);
        finder.findPath(abce.getNode("A"));
        finder.getPathString(abce.getNode("E"));
    }

    @Test(expected = PathFinder.PathNotFoundException.class)
    public void PathNotFoundWeighted() throws Exception {
        Graph abce = generateAbce();
        DijkstraPathFinder finder = new DijkstraPathFinder(abce);
        finder.findPath(abce.getNode("A"));
        finder.getPathString(abce.getNode("E"));
    }
}