package quicksort;

import static org.junit.Assert.*;
import org.junit.Test;

public class QuickSortTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowRuntimeExceptionWhenEmpty() {
        QuickSort<Integer> sorter = new QuickSort<>();
        Integer[] test = {};
        sorter.sort(test);
    }

    @Test
    public void testQuickSort() {
        Integer[] numbers = {30, 20, 10, 5, 6, 99};

        QuickSort<Integer> sorter = new QuickSort<>();
        sorter.sort(numbers);

        Integer[] expected = {5, 6, 10, 20, 30, 99};

        assertEquals(numbers.length, expected.length);
        for (int i = 0; i < numbers.length; i++) {
            assertEquals(numbers[i], expected[i]);
        }
    }

    @Test
    public void testWithNegativeSort() {
        Integer[] numbers = {-30, -20, 10, -5, 6, -99};

        QuickSort<Integer> sorter = new QuickSort<>();
        sorter.sort(numbers);

        Integer[] expected = {-99, -30, -20, -5, 6, 10};

        assertEquals(numbers.length, expected.length);
        for (int i = 0; i < numbers.length; i++) {
            assertEquals(numbers[i], expected[i]);
        }
    }

    @Test
    public void testWithDoubleSort() {
        Double[] numbers = {44.763, 50.923, 11.40, 44.76};

        QuickSort<Double> sorter = new QuickSort<>();
        sorter.sort(numbers);

        Double[] expected = {11.40, 44.76, 44.763, 50.923};

        assertEquals(numbers.length, expected.length);
        for (int i = 0; i < numbers.length; i++) {
            assertEquals(numbers[i], expected[i]);
        }
    }

    @Test
    public void testWithString(){
        String[] numbers = {"abc", "aaaa", "zaz", "AAA"};

        QuickSort<String> sorter = new QuickSort<>();
        sorter.sort(numbers);

        String[] expected = {"AAA", "aaaa", "abc", "zaz"};

        assertEquals(numbers.length, expected.length);
        for (int i = 0; i < numbers.length; i++) {
            assertEquals(numbers[i], expected[i]);
        }
    }
}