package binarySearchTree;

import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTreeTest {

    private BinarySearchTree<Integer, Integer> tree;

    @Test
    public void testAddLeft(){
        tree = new BinarySearchTree<>();
        tree.addToTree(10, 10);
        tree.addToTree(9, 9);

        assertTrue(tree.hasLeft());
        assertFalse(tree.hasRight());
    }

    @Test
    public void testHasChildren(){
        tree = new BinarySearchTree<>();
        assertFalse(tree.hasBoth());
        assertFalse(tree.hasLeft());
        assertFalse(tree.hasRight());

        tree.addToTree(10, 10);
        assertNotNull(tree.getValue());

        tree.addToTree(9, 9);
        assertTrue(tree.hasLeftOrRight());
        assertTrue(tree.hasLeft());

        tree.addToTree(11, 11);
        assertTrue(tree.hasBoth());
        assertTrue(tree.hasRight());
    }

    @Test
    public void testAddRight(){
        tree = new BinarySearchTree<>();
        tree.addToTree(10, 10);
        tree.addToTree(11, 11);
        assertTrue(tree.hasRight());
        assertFalse(tree.hasLeft());
    }

    @Test
    public void testFind(){
        tree = new BinarySearchTree<>();
        tree.addToTree(10, 10);
        tree.addToTree(9, 9);
        tree.addToTree(12, 12);
        tree.addToTree(14, 14);
        tree.addToTree(13, 13);

        assertNull(tree.findInTree(8));
        assertEquals(12, (int) tree.findInTree(12));
    }

    @Test
    public void testMinMax(){
        tree = new BinarySearchTree<>();
        tree.addToTree(10, 10);
        tree.addToTree(9, 9);
        tree.addToTree(8, 8);
        tree.addToTree(7, 7);
        assertEquals(7, (int)tree.getMin());
        assertEquals(10, (int)tree.getMax());
    }

    @Test
    public void testRemove(){
        tree = new BinarySearchTree<>();
        tree.addToTree(7, 7);
        tree.removeFromTree(7);

        assertNull(tree.findInTree(7));
    }

}