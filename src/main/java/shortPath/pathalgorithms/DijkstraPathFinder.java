package shortPath.pathalgorithms;

import shortPath.Edge;
import shortPath.Node;
import shortPath.Graph;

import java.util.PriorityQueue;

public class DijkstraPathFinder extends PathFinder {

    /*

    Aan de hand van onderstaande bronnen heb ik de implementatie gemaakt van het dijkstra algoritme.
    Dit is ook meteen de implementatie van de gewogen paden

    bronnen:
    https://stackabuse.com/graphs-in-java-dijkstras-algorithm/
    https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-in-java-using-priorityqueue/
     */

    public DijkstraPathFinder(Graph grah) {
        super(grah);
    }

    @Override
    public void findPath(Node start) {
        super.findPath(start);
        PriorityQueue<Edge> toVisitEdges = new PriorityQueue<>();

        int nodesSeen = 0;
        Node curNode = start;
        do {
            PathNodeInfo curNodeInfo = getNodeInfo(curNode);
            if (curNodeInfo.isProcessed)
                continue;

            curNodeInfo.isProcessed = true;
            nodesSeen++;

            for (Edge adjacentPath : curNode.adj) {
                Node adjDest = adjacentPath.destination;
                PathNodeInfo adjacentNodeDistance = getNodeInfo(adjDest);

                if (adjacentPath.weight < 0)
                    throw new IllegalStateException("Graph needs a minimum of one edge");

                if (adjacentNodeDistance.distance == -1 ||
                        curNodeInfo.distance + adjacentPath.weight < adjacentNodeDistance.distance) {
                    adjacentNodeDistance.distance = curNodeInfo.distance + adjacentPath.weight;
                    adjacentNodeDistance.prev = curNode;
                    adjacentNodeDistance.edgeToPrev = adjacentPath;
                    toVisitEdges.add(new Edge(adjDest, adjacentNodeDistance.distance));
                }
            }
        } while (!toVisitEdges.isEmpty() &&
                (curNode = toVisitEdges.remove().destination) != null
                && nodesSeen < grah.getNodeCount());
    }
}
