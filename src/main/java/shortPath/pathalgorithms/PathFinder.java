package shortPath.pathalgorithms;

import shortPath.Edge;
import shortPath.Graph;
import shortPath.Node;
import shortPath.PathElement;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public abstract class PathFinder {

    private Map<Node, PathNodeInfo> pathInformation = null;
    private Node startNode = null;
    Graph grah;

    public static class PathNotFoundException extends Exception {
    }

    PathFinder(Graph grah) {
        this.grah = grah;
        pathInformation = new HashMap<>();
    }

    protected void findPath(Node start) {
        this.startNode = start;
        pathInformation.clear();
    }

    private PathElement[] getPath(Node dest) throws PathNotFoundException {
        if (startNode == null)
            throw new IllegalStateException("No findPath run yet");

        LinkedList<PathElement> elements = new LinkedList<>();
        elements.add(dest);

        Node curNode = dest;

        while (curNode != null && curNode != startNode) {
            PathNodeInfo destInfo = getNodeInfo(curNode);

            if (destInfo.distance == -1)
                throw new PathNotFoundException();

            elements.addFirst(destInfo.edgeToPrev);
            elements.addFirst(destInfo.prev);

            curNode = destInfo.prev;
        }

        return elements.toArray(new PathElement[0]);
    }

    public String getPathString(Node dest) throws PathNotFoundException {
        PathElement[] path = getPath(dest);

        StringBuilder b = new StringBuilder();
        for (int i = 0; ; i++) {
            if (path[i] instanceof Edge)
                b.append(((Edge) path[i]).weight);
            else if (path[i] instanceof Node)
                b.append(((Node) path[i]).name);
            if (i + 1 == path.length)
                return b.toString();
            b.append("->");
        }
    }

    PathNodeInfo getNodeInfo(Node curNode) {
        if (pathInformation.containsKey(curNode))
            return pathInformation.get(curNode);
        PathNodeInfo pathNodeInfo = new PathNodeInfo();
        pathInformation.put(curNode, pathNodeInfo);
        return pathNodeInfo;
    }

    static class PathNodeInfo {
        int distance;
        Edge edgeToPrev;
        Node prev;

        PathNodeInfo() {
            reset();
        }

        int scratch;
        boolean isProcessed;


        void reset() {
            distance = -1;
            prev = null;
            scratch = 0;
            isProcessed = false;
            edgeToPrev = null;
        }

        @Override
        public String toString() {
            return "Info[" +
                    "dist=" + distance +
                    ", prev=" + prev +
                    ", edgeToPrev=" + edgeToPrev +
                    ", scratch=" + scratch +
                    ", isProcessed=" + isProcessed + ']';

        }
    }
}
