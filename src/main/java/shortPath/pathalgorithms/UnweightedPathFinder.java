package shortPath.pathalgorithms;

import shortPath.Edge;
import shortPath.Node;
import shortPath.Graph;

import java.util.LinkedList;
import java.util.Queue;

public class UnweightedPathFinder extends PathFinder{

    public UnweightedPathFinder(Graph grah) {
        super(grah);
    }

    @Override
    public void findPath(Node start) {
        super.findPath(start);

        Queue<Node> q = new LinkedList<>();
        Node curNode = start;
        do{
            PathNodeInfo curNodeInfo = getNodeInfo(curNode);
            for (Edge e : curNode.adj) {
                Node adjNode = e.destination;
                PathNodeInfo adjNodeInfo = getNodeInfo(adjNode);

                if (adjNodeInfo.distance == -1) {
                    adjNodeInfo.distance = curNodeInfo.distance + 1;
                    adjNodeInfo.edgeToPrev = e;
                    adjNodeInfo.prev = curNode;
                    q.add(adjNode);
                }
            }
        }while (!q.isEmpty() && (curNode = q.remove()) != null);
    }
}
