package shortPath;

public class Edge extends PathElement implements Comparable<Edge> {

    public Node destination;
    public int weight;

    public Edge(Node d, int c) {
        destination = d;
        weight = c;
    }

    @Override
    public int compareTo(Edge other) {
        return Integer.compare(weight, other.weight);
    }

    @Override
    public String toString() {
        return "Edge: destination: " + destination + ", cost=" + weight;
    }
}