package shortPath;

import java.util.LinkedList;
import java.util.List;

public class Node extends PathElement {

    public String name;

    public List<Edge> adj;

    Node(String nm) {
        name = nm;
        adj = new LinkedList<>();
    }



    @Override
    public String toString() {
        return "node: name= "  + name + ",adj=" + adj;
    }
}