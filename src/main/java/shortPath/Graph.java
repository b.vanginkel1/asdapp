package shortPath;

import java.util.*;

public class Graph {

    private Map<String, Node> nodes = new ChainingHashMap<>();

    Node getNode(String nodeName) {
        Node node = nodes.get(nodeName);
        if (node == null)
            throw new NoSuchElementException("Start node not found");
        return node;
    }

    public int getNodeCount() {
        return nodes.size();
    }

    Node getAndAddNode(String nodeName) {
        Node node = nodes.get(nodeName);
        if (node == null) {
            node = new Node(nodeName);
            nodes.put(nodeName, node);
        }
        return node;
    }

    private Collection<Node> getNodes() {
        return nodes.values();
    }

    void addEdge(String sourceName, String destName, int cost) {
        Node s = getAndAddNode(sourceName);
        Node d = getAndAddNode(destName);
        s.adj.add(new Edge(d, cost));
    }

    /**
     * Are all nodes in this graph connected. The direction is ignored.
     */
    boolean isConnected() {
        if (nodes.size() <= 1)
            return true;
        Collection<Node> connectedNodes = new ArrayList<>();
        connectedNodes.add((Node) nodes.values().toArray()[0]);

        boolean addedOne;
        do {
            addedOne = false;
            for (Node checkingNode : nodes.values()) {
                for (Edge edge : checkingNode.adj) {
                    if (connectedNodes.contains(checkingNode)) {
                        if (!connectedNodes.contains(edge.destination)) {
                            connectedNodes.add(edge.destination);
                            addedOne = true;
                        }
                    } else {
                        for (Node connectedNode : connectedNodes) {
                            if (edge.destination == connectedNode) {
                                if (!connectedNodes.contains(checkingNode)) {
                                    connectedNodes.add(checkingNode);
                                    addedOne = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } while (addedOne);

        return connectedNodes.size() == nodes.size();
    }

}
