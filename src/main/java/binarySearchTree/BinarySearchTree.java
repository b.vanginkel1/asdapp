package binarySearchTree;

import java.security.InvalidParameterException;

public class BinarySearchTree<T extends Comparable<? super T>, Y> {

    private final static String IN = "in";
    private final static String POST = "post";
    private final static String PRE = "pre";

    private T key;
    private Y value;
    private BinarySearchTree<T, Y> left;
    private BinarySearchTree<T, Y> right;
    private int subTreeValue;

    BinarySearchTree() {}

    private BinarySearchTree(T key, Y value) {
        this.key = key;
        this.value = value;
        subTreeValue = 0;
    }

    private Boolean isLeaf() { return left == null && right == null; }

    Y getValue() {
        return value;
    }

    boolean hasLeft() {
        return left != null;
    }

    boolean hasRight(){
        return right != null;
    }

    boolean hasBoth(){
        return left != null && right != null;
    }

    boolean hasLeftOrRight(){
        return left != null || right != null;
    }

    void addToTree(T other, Y value) {
        if(key == null) {
            key = other;
            this.value = value;
        }

        if(key.compareTo( other ) > 0) {
            addLeftToTree( other, value );
        } else if(key.compareTo( other ) < 0) {
            addRightToTree( other, value );
        }
    }

    private void addLeftToTree(T other, Y value) {
        if(left == null) {
            left = new BinarySearchTree<>( other, value );
        } else {
            left.addToTree(other, value);
        }
    }

    private void addRightToTree(T other, Y value) {
        if(right == null) {
            right = new BinarySearchTree<>( other, value );
        } else {
            right.addToTree(other, value);
        }
    }

    Y findInTree(T k) {
        if(isLeaf()) return null;
        int factor = k.compareTo(key);
        if(factor == 0) return value;
        return (factor > 0) ? right.findInTree(k) : left.findInTree(k);
    }

    private BinarySearchTree<T, Y> findTree(T k) {
        if(isLeaf()) return null;
        int factor = key.compareTo(k);
        if(factor == 0) return this;
        return (factor < 0) ? right.findTree(k) : left.findTree(k);
    }

    Y getMax() {
        return right == null ? value : right.getMax();
    }

    private BinarySearchTree<T, Y> getMaxTree() {
        return right == null ? this : right.getMaxTree();
    }

    Y getMin() {
        return left == null ? value : left.getMin();
    }

    private BinarySearchTree<T, Y> getMinTree() {
        return left == null ? this : left.getMinTree();
    }

    BinarySearchTree<T, Y> removeFromTree(T k) {
        int compareFactor = k.compareTo( key );

        if(isLeaf()) return null;

        if(compareFactor == 0) {
            //0 children
            if(isLeaf())
                return null;

            //2 children
            if(left != null && right != null) {
                BinarySearchTree<T, Y> max = left.getMaxTree();
                removeFromTree( max.key );
                max.right = right;
                max.left = left;
                return max;
            }

            //1 child
            if(left != null)
                return left;

            if(right != null)
                return right;
        }

        if(compareFactor < 0)
            left = left.removeFromTree( k );

        if(compareFactor > 0)
            right = right.removeFromTree( k );

        return this;
    }

    private int calculateSubTreeValues() {
        resetSubTreeValues();
        if(isLeaf()) {
            subTreeValue = 0;
            return 1 + subTreeValue;
        }

        if(right != null && left == null) {
            subTreeValue += right.calculateSubTreeValues();
            return 1 + subTreeValue;
        }

        if(left != null && right == null) {
            subTreeValue += left.calculateSubTreeValues();
            return 1 + subTreeValue;
        }

        subTreeValue += right.calculateSubTreeValues() + left.calculateSubTreeValues();
        return 1 + subTreeValue;
    }

    private void resetSubTreeValues() {
        subTreeValue = 0;
        if(right != null)
            right.resetSubTreeValues();
        if(left != null)
            left.resetSubTreeValues();
    }

    private int nLeaves() {
        if(isLeaf())
            return 1;

        if(right == null && left != null)
            return left.nLeaves();

        if(right != null && left == null)
            return right.nLeaves();

        return right.nLeaves() + left.nLeaves();
    }

    private String toString(int depth, String order) {
        calculateSubTreeValues();
        String stringToReturn = "";
        String spacing = "";

        if(depth != -1) {
            depth++;
            for (int i = 0; i < depth; i++) {
                spacing += "\t";
            }
        }
        switch (order) {
            case IN:
                stringToReturn = getInOrder(depth, stringToReturn, spacing);
                break;
            case POST:
                stringToReturn = getPostOrder(depth, stringToReturn, spacing);
                break;
            case PRE:
                stringToReturn = getPreOrder(depth, stringToReturn, spacing);
                break;
            default:
                throw new InvalidParameterException("Not known order!");
        }
        return stringToReturn;
    }

    private String getInOrder(int depth, String result, String tab) {
        result = left(depth, result, tab, IN);
        result = current(depth, result, tab);
        result = right(depth, result, tab, IN);
        return result;
    }

    private String getPreOrder(int depth, String result, String tab) {
        result = left(depth, result, tab, PRE);
        result = current(depth, result, tab);
        result = right(depth, result, tab, PRE);
        return result;
    }

    private String getPostOrder(int depth, String result, String tab) {
        result = left(depth, result, tab, POST);
        result = current(depth, result, tab);
        result = right(depth, result, tab, POST);
        return result;
    }

    private String current(int depth, String result, String tab) {
        result += key;
        result += (depth == -1) ? " " : "\n" + tab;
        return result;
    }

    private String left(int depth, String result, String tab, String order) {
        result += (left == null) ? "" : tab + left.toString(depth, order);
        result += (depth == -1) ? " " : "\n" + tab;
        return result;
    }

    private String right(int depth, String result, String tab, String in) {
        result += (right == null) ? "" : tab + right.toString(depth, in);
        result += (depth == -1) ? " " : "\n" + tab;
        return result;
    }
}